About
-----
Vostok is a small set of customizations for the Android Open Source Project.
Unlike many other projects, it's just a set of patches on top of AOSP. Only
those components have been forked because Google abandoned them:

* Gallery
* Calendar

Supported Devices
-----------------
| Name                         | Codename        | Branch            | Build  |
| ---------------------------- | --------------- | ----------------- | ------ |
| LG Nexus 4                   | mako            | android-5.1.1_r35 | LMY49H |

Building
--------
Select branch and codename for your device from the table above and run:

    repo init -u https://android.googlesource.com/platform/manifest -b branch
    mkdir -p .repo/local_manifests
    curl https://gitlab.com/vostok/vostok-5/raw/master/vostok.xml > .repo/local_manifests/vostok.xml
    repo sync
    source build/envsetup.sh
    lunch aosp_codename-userdebug
    make -j16

Running
-------
Unlock the bootloader of your device, boot it into the fastboot mode and flash
images:

    fastboot boot boot.img
    fastboot system system.img
    fastboot userdata userdata.img
    fastboot format cache
    fastboot reboot
